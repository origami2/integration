function StubTokenProvider1() {
}

StubTokenProvider1.prototype.getUserToken = function (username, password) {
  if (password !== 'letmepass') return Promise.reject('password must be letmepass');
  
  return Promise.resolve({
    username: username
  });
};

module.exports = StubTokenProvider1;