function StubPlugin2(username) {
  this.username = username;
}

StubPlugin2.prototype.whoAmI = function () {
  return Promise.resolve(this.username);
};

module.exports = StubPlugin2;